const Blockchain = require('../blockchain');
const blockchain = new Blockchain();

let prevTimestamp, nextTimestamp, nextBlock, timeDiff, average;
const times = [];

blockchain.addBlock({ data: 'initial' })

console.log(blockchain.chain[0])

for (let i = 0; i < 10000; i++) {
  prevTimestamp = blockchain.chain[blockchain.chain.length -1].timestamp;
  blockchain.addBlock({ data: `block${i}`});
  nextBlock = blockchain.chain[blockchain.chain.length - 1];
  nextTimestamp = nextBlock.timestamp;
  timeDiff = nextTimestamp - prevTimestamp;
  times.push(timeDiff);

  average = times.reduce(function(total, num){ return total + num })/times.length;
  console.log(`Time to mine block ${timeDiff}ms. Difficulty: ${nextBlock.difficulty}. Average time: ${average}ms.`)
}
