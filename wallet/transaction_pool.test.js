const TransactionPool = require('./transaction_pool');
const Transaction = require('./transaction');
const Wallet = require('./index');
const Blockchain = require('../blockchain');

describe('TransactionPool', function() {
  let transaction, transactionPool, senderWallet;

  beforeEach(function () {
    transactionPool = new TransactionPool();
    senderWallet= new Wallet()
    transaction = new Transaction({
      senderWallet,
      recipient: 'fake-recipient',
      amount: 50
    });
  })

  describe('setTransaction', function() {
    it('adds a transaction', function() {
      transactionPool.setTransaction(transaction);
      expect(transactionPool.transactionMap[transaction.id]).toBe(transaction);
    })
  })

  describe('existingTransaction', function() {
    it('returns an existing transaction given an input address', function() {
      transactionPool.setTransaction(transaction);
      expect(
        transactionPool.existingTransaction({ inputAddress: senderWallet.publicKey })
      ).toBe(transaction)
    })
  })

  describe('validTransactions', function() {
    let validTransations, errorMock;

    beforeEach(function() {
      validTransactions = [];
      errorMock = spyOn(console, 'error');

      for (let i = 0; i < 10; i++) {
        transaction = new Transaction({
          senderWallet,
          recipient: 'any-recipeint',
          amount: 30
        })

        if (i % 3 === 0) {
          transaction.input.amount = 9999999;
        } else if (i % 3 === 1) {
          transaction.input.signature = new Wallet().sign('foo');
        } else {
          validTransactions.push(transaction)
        }

        transactionPool.setTransaction(transaction)
      }
    })

    it('returns valid transaction', function() {
      expect(transactionPool.validTransactions()).toEqual(validTransactions);
    })

    it('logs errors for invalid transactions', function() {
      transactionPool.validTransactions();
      expect(errorMock).toHaveBeenCalled()
    })
  })

  describe('clear', function() {
    it('clears the transactions', function() {
      transactionPool.clear();
      expect(transactionPool.transactionMap).toEqual({});
    })
  })

  describe('clearBlockchainTransactions', function() {
    it('clears the pool of any exisiting blockchain transactions', function() {
      const blockchain = new Blockchain();
      const expectedTransactionMap = {}

      for (let i = 0; i < 6; i++) {
        const transaction = new Wallet().createTransaction({
          recipient: 'foo', amount: 20
        });

        transactionPool.setTransaction(transaction);

        if (i % 2 === 0) {
          blockchain.addBlock({ data: [transaction] })
        } else {
          expectedTransactionMap[transaction.id] = transaction;
        }
      }

      transactionPool.clearBlockchainTransactions({ chain: blockchain.chain });

      expect(transactionPool.transactionMap).toEqual(expectedTransactionMap);
    })
  })
})
