const Wallet = require('./index')
const { verifySignature } = require('../util')
const Transaction = require('./transaction')

describe('Wallet', function() {
  let wallet;

  beforeEach(function() {
    wallet = new Wallet();
  })

  it('has a `balance`', function() {
    expect(Object.keys(wallet)).toContain('balance');
  })

  it('has a starting `balance`', function() {
    expect(wallet.balance).toEqual(1000);
  })

  it('has a `publicKey`', function() {
    expect(Object.keys(wallet)).toContain('publicKey');
  })

  describe('signing data', function(){
    let data;

    beforeEach(function() { data = 'foobar'; })

    it('verifies a signature', function() {
      expect(verifySignature({
        publicKey: wallet.publicKey,
        data,
        signature: wallet.sign(data)
      })).toBe(true)
    })

    it('does not verify an invalid signature', function() {
      expect(verifySignature({
        publicKey: wallet.publicKey,
        data,
        signature: new Wallet().sign(data)
      })).toBe(false)
    })
  })

  describe('createTransaction', function() {
    describe('and the amount is valid', function() {
      let transaction, senderWallet, recipient, amount;

      beforeEach(() => {
        amount = 50;
        recipient = 'recipient-public-key';
        transaction = wallet.createTransaction({ recipient, amount});
      });


      it('creates an instance of `Transaction`', function() {
        expect(transaction instanceof Transaction).toBe(true)
      })

      it('mathches the transaction input with the wallet', function() {
        expect(transaction.input.address).toEqual(wallet.publicKey);
      })

      it('outputs the amount the recipient', function() {
        expect(transaction.outputMap[recipient]).toEqual(amount);
      })
    })

    describe('and the amount exceeds the balance', function() {
      it('throws an error', function() {
        expect(() => wallet.createTransaction({ amount: 999999, recipient: 'foo-recipient' }))
          .toThrowError(Error, 'Amount exceeds balance');
      })
    })
  })
})
