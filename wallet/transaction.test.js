const Transaction = require('./transaction');
const Wallet = require('./index');
const { verifySignature } = require('../util');
const { REWARD_INPUT, MINING_REWARD } = require('../config')

describe('Transaction', () => {
  let transaction, senderWallet, recipient, amount;

  beforeEach(() => {
    senderWallet = new Wallet();
    recipient = 'recipient-public-key';
    amount = 50;

    transaction = new Transaction({ senderWallet, recipient, amount});
  });

  it('has an `id`', function() {
    expect(Object.keys(transaction)).toContain('id');
  });

  describe('outputMap', function() {
    it('has an `outputMat`', function() {
      expect(Object.keys(transaction)).toContain('outputMap')
    })

    it('outputs the amount to the recipient', function() {
      expect(transaction.outputMap[recipient]).toEqual(amount);
    })

    it('outputs the remaining balance for the `senderWallet`', function() {
      expect(transaction.outputMap[senderWallet.publicKey])
        .toEqual(senderWallet.balance - amount)
    });
  });

  describe('input', function() {
    it('has an `input`', function() {
      expect(Object.keys(transaction)).toContain('input');
    })

    it('has an `timestamp` in the `input`', function() {
      expect(Object.keys(transaction.input)).toContain('timestamp');
    })

    it('sets the `amount` to the `senderWallet` balance', function() {
      expect(transaction.input.amount).toEqual(senderWallet.balance);
    })

    it('set the `address` to the `senderWallet` publicKey', function() {
      expect(transaction.input.address) .toEqual(senderWallet.publicKey)
    });

    it('signs the input', function() {
      expect(verifySignature({
        publicKey: senderWallet.publicKey,
        data: transaction.outputMap,
        signature: transaction.input.signature
      })).toBe(true)
    });
  })

  describe('validTransaction', function() {
    let errorMock;

    beforeEach(function() {
      errorMock = spyOn(console, 'error')
    });

    describe('when the transaction is valid', function() {
      it('returns true', function() {
        expect(Transaction.validTransaction(transaction)).toBe(true);
      })
    })

    describe('when the transaction is invalid', function() {
      describe('and transaction outputMap valid is invalid', function() {
        it('returns false', function() {
          transaction.outputMap[senderWallet.publicKey] = 999999;
          expect(Transaction.validTransaction(transaction)).toBe(false);
          expect(errorMock).toHaveBeenCalled();
        })

      })

      describe('and transaction signature is invalid', function() {
        it('returns false', function() {
          transaction.input.signature = new Wallet().sign('data');
          expect(Transaction.validTransaction(transaction)).toBe(false);
          expect(errorMock).toHaveBeenCalled();
        })
      })
    })
  })

  describe('update', function() {
    let originalSignature, originalSenderOutput, nextRecipient, nextAmount;

    describe('and the amount is valid', function() { 
      beforeEach(function() {
        originalSiganture = transaction.input.signature;
        originalSenderOutput = transaction.outputMap[senderWallet.publicKey];
        nextRecipient = 'next-recipient'
        nextAmount = 49;

        transaction.update({ senderWallet, recipient: nextRecipient, amount: nextAmount });
      })

      it('outputs the amount to the next recipient', function() {
        expect(transaction.outputMap[nextRecipient]).toEqual(nextAmount);
      })

      it('subtracts the amount from the original sender output amount', function() {
        expect(transaction.outputMap[senderWallet.publicKey])
          .toEqual(originalSenderOutput - nextAmount);
      })

      it('maintains a total output that matches the input amount', function() {
        expect(
          Object.values(transaction.outputMap)
                .reduce((total, outputAmount) => total + outputAmount)
        ).toEqual(transaction.input.amount)
      })

      it('re-signs the transaction', function() {
        console.log(JSON.stringify(transaction.input.signature), JSON.stringify(originalSignature))
        expect(transaction.input.signature).not.toEqual(originalSignature);
      })

      describe('and another update for the same recipient', function() {
        let addedAmount;

        beforeEach(function() {
          addedAmount = 80;
          transaction.update({
            senderWallet, recipient: nextRecipient, amount: addedAmount
          })
        })

        it('adds the recipient amount', function() {
          expect(transaction.outputMap[nextRecipient]).toEqual(nextAmount + addedAmount);
        })

        it('subtracts the amount from the original sender output amount', function() {
          expect(transaction.outputMap[senderWallet.publicKey])
            .toEqual(originalSenderOutput - nextAmount - addedAmount);
        })
      })
    })
  })

  describe('and the amount is invalid', function() {
    it('throws an error', function() {
      expect(function() {
        transaction.update({
          senderWallet, recipient: 'foo', amount: 999999
        })
      }).toThrowError('Amount exceeds balance')
    })
  })

  describe('rewardTransaction', function() {
    let rewardTransaction, minerWallet;

    beforeEach(function() {
      minerWallet = new Wallet();
      rewardTransaction = Transaction.rewardTransaction({ minerWallet })
    })

    it('creates a transaction with the reward input', function() {
      expect(rewardTransaction.input).toEqual(REWARD_INPUT)
    })

    it('creates a transaction with the reward input', function() {
      expect(rewardTransaction.outputMap[minerWallet.publicKey]).toEqual(MINING_REWARD)
    })
  })
})
