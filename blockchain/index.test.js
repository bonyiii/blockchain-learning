const Blockchain = require('./index');
const Block = require('./block');
const { cryptoHash } = require('../util');

describe('Blockchain', function() {
  let blockchain;

  beforeEach(function() {
    blockchain = new Blockchain();
  })

  it('contains a chain Array instance', function() {
    expect(blockchain.chain instanceof Array).toBe(true);
  })

  it('starts with the genesis block', function() {
    expect(blockchain.chain[0]).toEqual(Block.genesis());
  })

  it('adds a new block to the chain', function() {
    const newData = 'foo bar';
    blockchain.addBlock({ data: newData });

    expect(blockchain.chain[blockchain.chain.length - 1].data).toEqual(newData);
  });

  describe('isValidChain', function() {
    beforeEach(function() {
      blockchain.addBlock({ data: 'Bears'})
      blockchain.addBlock({ data: 'Beets'})
      blockchain.addBlock({ data: 'Battlestar Galactica'})
    })

    it("returns true", function() {
      expect(Blockchain.isValidChain(blockchain.chain)).toBe(true)
    });

    describe("when the chain doesn't start with genesis block", function() {
      it("returns false", function() {
        blockchain.chain[0] = { data: 'fake-genesis'}
        expect(Blockchain.isValidChain(blockchain.chain)).toBe(false)
      });
    })

    describe("when the chain start with genesis block", function() {
      describe("and has multiple blocks", function() {
        describe("and lastHash reference has changed", function() {
          it("returns false", function() {
            blockchain.chain[2].lastHash = 'broken-lastHash';
            expect(Blockchain.isValidChain(blockchain.chain)).toBe(false)
          });
        })

        describe("and the chain contains a block with invalid field", function() {
          it("returns false", function() {
            blockchain.chain[2].data = 'evil-data';
            expect(Blockchain.isValidChain(blockchain.chain)).toBe(false)
          });
        });
      });

      describe("and the chain contains a jumped difficulty", function() {
        it('returns false', function() {
          const lastBlock = blockchain.chain[blockchain.chain.length - 1]
          const lastHash = lastBlock.hash
          const timestamp = Date.now();
          const nonce = 0;
          const data = [];
          const difficulty = lastBlock.difficulty - 3;

          const hash = cryptoHash(timestamp, lastHash, difficulty, nonce, data);

          const badBlock = new Block({ timestamp, lastHash, nonce, difficulty, data })

          blockchain.chain.push(badBlock);

          expect(Blockchain.isValidChain(blockchain.chain)).toBe(false);
        });
      });
    });
  });

  describe('replaceChain', function() {
    let newChain, originalChain, errorMock, logMock;
    beforeEach(function() {
      originalChain = blockchain.chain;
      newBlockchain = new Blockchain();
      errorMock = spyOn(console, 'error')
      logMock = spyOn(console, 'log')
    });

    describe('when the new chain is not longer', function() {
      beforeEach(function() {
        newBlockchain.chain[0] = { new: 'chain' };
        blockchain.replaceChain(newBlockchain.chain);
      })

      it('does not replace the chain', function() {
        expect(originalChain).toEqual(blockchain.chain);
      });

      it('logs an error', function() {
        expect(errorMock).toHaveBeenCalled();
      })
    })

    describe('when the new chain is longer', function() {
      beforeEach(function() {
        newBlockchain.addBlock({ data: 'Bears'})
        newBlockchain.addBlock({ data: 'Beets'})
        newBlockchain.addBlock({ data: 'Battlestar Galactica'})
      })

      describe('and the it is invalid', function() {
        beforeEach(function() {
          newBlockchain.chain[2].hash = 'some-fake-hash';
          blockchain.replaceChain(newBlockchain.chain);
        })

        it('does not replace the chain', function() {
          expect(blockchain.chain).toEqual(originalChain);
        });

        it('logs an error', function() {
          expect(errorMock).toHaveBeenCalled();
        })
      })

      describe('and the it is valid', function() {
        beforeEach(function() {
          blockchain.replaceChain(newBlockchain.chain);
        })

        it('replace the chain', function() {
          expect(blockchain.chain).toEqual(newBlockchain.chain);
        });

        it('logs to console', function() {
          expect(logMock).toHaveBeenCalled();
        })
      });
    });
  });
});
