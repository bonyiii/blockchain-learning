const Block = require('./block');
const { cryptoHash } = require('../util');

class Blockchain {
  constructor() {
    this.chain = [Block.genesis()];
  }

  static isValidChain(chain) {
    if (JSON.stringify(chain[0]) !== JSON.stringify(Block.genesis())) {
      return false;
    }

    for(let i = 1; i < chain.length; i++) {
      const { timestamp, lastHash, hash, data, nonce, difficulty } = chain[i];
      const prevDifficulty = chain[i-1].difficulty;
      const prevLastHash = chain[i-1].hash;

      if (Math.abs(difficulty - prevDifficulty) > 1) { return false; }
      if (lastHash !== prevLastHash) { return false; }
      if (hash !== cryptoHash(timestamp, lastHash, data, nonce, difficulty)) { return false; }
    }

    return true;
  }

  addBlock({ data }) {
    const newBlock = Block.mineBlock({
      lastBlock: this.chain[this.chain.length - 1],
      data
    })
    this.chain.push(newBlock);
  }

  replaceChain(chain, onSuccess) {
    if (chain.length <= this.chain.length) {
      console.error('The incoming chain must be longer');
      return;
    }

    if (!Blockchain.isValidChain(chain)) {
      console.error('The incoming chain must be valid');
      return;
    }

    if (onSuccess) { onSuccess(); }
    console.log('replacing chain with', chain)
    this.chain = chain;
  }
}

module.exports = Blockchain
