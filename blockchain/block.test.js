const hexToBinary = require('hex-to-binary');
const Block = require('./block.js');
const { cryptoHash } = require('../util');
const { GENESIS_DATA, MINE_RATE } = require('../config')

describe('Block', function() {
  let timestamp, lastHash, hash, data, block, nonce, difficulty;
  beforeEach(function() {
    nonce = 1;
    difficulty = 1;
    timestamp = 2000;
    lastHash = 'foo-hash';
    hash = 'bar-hash';
    data = ['blockchain', 'data'];
    block = new Block({ timestamp, lastHash, hash, data, nonce, difficulty });
  })

  it('has a timestamp, lasthash, hash and data property', function() {
    expect(block.timestamp).toEqual(timestamp);
    expect(block.lastHash).toEqual(lastHash);
    expect(block.hash).toEqual(hash);
    expect(block.data).toEqual(data);
    expect(block.nonce).toEqual(nonce);
    expect(block.difficulty).toEqual(difficulty);
  });

  describe('genesis', function() {
    beforeEach(function() {
      this.genesisBlock = Block.genesis();
    })

    it('returns a Blcok instance', function() {
      expect(this.genesisBlock instanceof Block).toBe(true)
    });

    it('returns the genesis data', function() {
      //expect(this.genesisBlock).toEqual(GENESIS_DATA);
    })
  });

  describe('mineBlock', function() {
    let lastBlock, data, minedBlock;
    beforeEach(function() {
      lastBlock = Block.genesis();
      data = 'mined data';
      minedBlock = Block.mineBlock({ lastBlock, data });
    });

    it('returns a Block instanec', function() {
      expect(minedBlock instanceof Block).toBe(true)
    });

    it('set last and has of the lastBlock', function() {
      expect(minedBlock.lastHash).toEqual(lastBlock.hash);
    });

    it('sets the data', function() {
      expect(minedBlock.data).toEqual(data);
    });

    it('sets a timestamp', function() {
      expect(minedBlock.timestamp).not.toEqual(undefined);
    });

    it('creates a SHA-256 hash based on the proper inputs', function() {
      expect(minedBlock.hash).toEqual(cryptoHash(
        minedBlock.timestamp,
        minedBlock.nonce,
        minedBlock.difficulty,
        lastBlock.hash,
        data,
      ));
    });

    it('sets a hash that matches the difficulty criteria', function() {
      expect(hexToBinary(minedBlock.hash).substring(0, minedBlock.difficulty))
        .toEqual('0'.repeat(minedBlock.difficulty))
    });

    it('adjusts the difficulty', function() {
      const possibleResults = [lastBlock.difficulty + 1, lastBlock.difficulty - 1];
      expect(possibleResults.includes(minedBlock.difficulty)).toBe(true);
    })
  });

  describe('adjustDifficulty', function() {
    it('raises the diffculty for a quickly mined block', function() {
      expect(Block.adjustDifficulty({
        originalBlock: block,
        timestamp: block.timestamp + MINE_RATE - 100
      })).toEqual(block.difficulty + 1)
    });

    it('raises the diffculty for a slowly mined block', function() {
      expect(Block.adjustDifficulty({
        originalBlock: block,
        timestamp: block.timestamp + MINE_RATE + 100
      })).toEqual(block.difficulty - 1)
    });

    it('has a lower limit of 1', function() {
      block.difficulty = -1;
      expect(Block.adjustDifficulty({ originalBlock: block })).toEqual(1)
    });
  });
});
