const bodyParser = require('body-parser');
const express = require('express');
const request = require('request');

const Blockchain = require('./blockchain');
const PubSub = require('./app/pubsub');
const TransactionPool = require('./wallet/transaction_pool');
const Wallet = require('./wallet')
const TransactionMiner = require('./app/transaction_miner')

const app = express();
const blockchain = new Blockchain();
const transactionPool = new TransactionPool();
const wallet = new Wallet();
const pubsub = new PubSub({ blockchain, transactionPool });
const transactionMiner = new TransactionMiner({ blockchain, transactionPool, wallet, pubsub });

const DEFAULT_PORT = 8080;
const ROOT_NODE_ADDRESS = `http://localhost:${DEFAULT_PORT}`

setTimeout(() => pubsub.broadcastChain(), 1000);
app.use(bodyParser.json())

app.get('/api/blocks', function(req, res) {
  res.json(blockchain.chain);
});

app.post('/api/mine', function(req, res) {
  const { data } = req.body;

  blockchain.addBlock({ data });
  pubsub.broadcastChain();

  res.redirect('/api/blocks');
});

app.post('/api/transact', function(req, res) {
  const { amount, recipient } = req.body;
  let transaction = transactionPool.existingTransaction({ inputAddress: wallet.publicKey });

  try {
    if (transaction) {
      transaction.update({ senderWallet: wallet, recipient, amount });
    } else {
      transaction = wallet.createTransaction({ recipient, amount });
    }
  } catch(error) {
    return res.status(400).json({ type: 'error', message: error });
  }

  transactionPool.setTransaction(transaction);
  pubsub.broadcastTransaction(transaction);

  res.json({ type: 'success', transaction });
});

app.get('/api/transaction-pool-map', function(req, res) {
  res.json(transactionPool.transactionMap);
})

app.get('/api/mine-transaction', function(req, res) {
  transactionMiner.mineTransaction();

  res.redirect('/api/blocks');
})

const syncWithRootState = () => {
  request({ url: `${ROOT_NODE_ADDRESS}/api/blocks` }, (error, response, body) => {
    if (!error && response.statusCode === 200) {
      const rootChain = JSON.parse(body);

      console.log('replace chain on startup sync with', rootChain);
      blockchain.replaceChain(rootChain);
    }
  });

  request({ url: `${ROOT_NODE_ADDRESS}/api/transaction-pool-map` }, (error, response, body) => {
    if (!error && response.statusCode === 200) {
      const rootTransactionPoolMap = JSON.parse(body)

      console.log('replace transaction pool map on sync with', rootTransactionPoolMap);
      transactionPool.setMap(rootTransactionPoolMap);
    }
  })
};

let peer_port;

if (process.env.GENERATE_PEER_PORT === 'true') {
  peer_port = DEFAULT_PORT + Math.ceil(Math.random() * 1000);
}

const port = peer_port || DEFAULT_PORT;
app.listen(port, function() {
  if (port != DEFAULT_PORT) { syncWithRootState(); }
  console.log(`listening on port: ${port}`);
});
