const crypto = require('crypto');
const EC = require('elliptic').ec;

const ec = new EC('secp256k1');

const verifySignature = ({ publicKey, data, signature}) => {
  const keyFromPublic = ec.keyFromPublic(publicKey, 'hex');

  return keyFromPublic.verify(cryptoHash(data), signature)
}

const cryptoHash = function(...inputs) {
  const hash = crypto.createHash('sha256');
  hash.update(inputs.map(function(input) {
    return JSON.stringify(input);
  }).sort().join(' '));
  return hash.digest('hex');
}

module.exports = { cryptoHash, ec, verifySignature };

