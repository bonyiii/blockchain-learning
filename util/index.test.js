const { cryptoHash } = require('./index');

describe('cryptoHash', function() {
  it('generates a SHA-256 hashed output', function() {
    expect(cryptoHash('Hello blockchain course'))
      .toEqual('250afb82823a8042bae7f7edfe75783848b036db61725ad85969348aa71da0cc');
  });

  it('produces the same hash with same args in any order', function() {
    expect(cryptoHash('one', 'two', 'three'))
      .toEqual(cryptoHash('two', 'one', 'three'));
  });

  it('produces a unique hash when properties have changed on an input', function() {
    const foo = {};
    const originalHash = cryptoHash(foo);
    foo['a'] = 'a';
    expect(cryptoHash(foo)).not.toEqual(originalHash);
  })
});
